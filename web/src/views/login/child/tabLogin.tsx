import { defineComponent, toRefs, computed } from "vue"
import SvgIcon from "@/components/module/SvgIcon/index.vue"
import { authPinia } from '../config'
export default defineComponent({
    props: {
        active: {
            type: Number || String,
        }
    },
    setup(props, { emit }) {
        const authStore = authPinia()
        const { active } = toRefs(props)
        const primaryColor = computed(() => authStore.color)
        const iconList = ['login_user', 'login_phone', 'login_erCode']
        const handleTabClick = (i: number) => emit('update:active', i)
        return () => (<div style={{ textAlign: 'right' }}>
            <a-space size='small'>
                {iconList.map((item, i) => (<SvgIcon name={item} color={active.value === i ? primaryColor.value : 'inherit'} onClick={() => handleTabClick(i)} />))}
            </a-space>
        </div>)
    }
})