import { authPinia } from '@/stores'
export const rules = {
    username: [{ required: true, message: "请输入用户名！", trigger: "change" }],
    password: [{ required: true, message: "请输入密码！", trigger: "change" }],
    phone: [
        { required: true, message: "请输入手机号！", trigger: "change" },
        { pattern: /^1[3-9]\d{9}$/, message: "手机号格式错误！", trigger: "change" }
    ],
    phoneCode: [
        { required: true, message: "请输入验证码！", trigger: "change" },
        { min: 6, max: 6, message: "验证码长度为6！", trigger: "change" },
        { pattern: /^[a-zA-Z0-9]*$/, message: "验证码格式错误！", trigger: "change" }
    ]
};
export { authPinia }