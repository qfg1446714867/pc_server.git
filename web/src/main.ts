import { createApp } from 'vue'
import App from './App.vue'
import './assets/css/style.css'
import 'virtual:svg-icons-register' // 自定义svg图标
import { createPinia } from 'pinia'
import router from './router'
import './router/permission'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/reset.css';
import "ol/ol.css";
import DataVVue3 from '@kjgl77/datav-vue3'
const app = createApp(App)
app.use(createPinia())
app.use(router)
app.use(Antd)
app.use(DataVVue3)
app.mount('#app')
