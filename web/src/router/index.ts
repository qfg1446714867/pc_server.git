import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
// vite-env.d.ts
const Login = () => import('@/views/login/index.vue')
const Layout = () => import('@/components/module/Layout/index.vue')
const OneClickLogin = () => import('@/components/module/oneClickLogin/index.vue')
const NotFound404 = () => import('@/views/not-found/404.tsx')
const routes: RouteRecordRaw[] = [
    {
        path: '/login',
        name: 'LOGIN',
        meta:{
            title:'登录',
            label:'登录',
        },
        component: Login,
    },
    {
        path: '/',
        name: 'LAYOUT',
        redirect: '/home',
        component: Layout,
        children: []
    },
    {
        path: '/404',
        name: '404',
        component: NotFound404,
    },
    {
        path: '/oneClickLogin',
        name: 'ONECLICKLOGIN',
        component: OneClickLogin,
    }
]
const router = createRouter({
    history: createWebHistory('custom'),
    routes,
})
export default router