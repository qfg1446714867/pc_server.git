import { authPinia, dataPinia } from '@/stores'
export { message as AntMessage } from 'ant-design-vue';
export { defaultPathActive, deepClone, colorOpacity } from '@/utils'
export { fileGoblob ,blobGoBase64} from '@/utils'
const authStore = authPinia()
export const { tabTransNum, showHead, showBread, defaultMenuCollapsed } = authStore.providerOptions.layoutOption
export const { colorBar: barColor, colorError: errorColor, colorWarning: wraningColor } = authStore.providerOptions.theme.token
export const headList = [
    { label: '个人中心', value: 'personal' },
    { label: '退出登陆', value: 'exit' },
]
export const forRouter = (list) => {
    const arr = list.map((item) => {
        let newItem: any = {
            key: item.path,
            label: item.title,
            title: item.title,
            icon: item.icon,
        }
        if (item.children && item.children.length > 0) {
            newItem.children = forRouter(item.children)
        }
        if (item.show === 1) {
            return newItem
        }
    }).filter((item) => item)
    return arr
}
// 旋转菜单
export const menusList = forRouter(authStore.menuList) || []
// 旋转菜单跳转参数
export const circleRouter = (row: any) => {
    return {
        path: row.path || row.key
    }
}
export { authPinia, dataPinia }