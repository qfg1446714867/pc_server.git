/**
 * 跨标签通讯---存
 * @param {string} name 缓存的keyName
 * @param {*} data 缓存fun 缓存值 
 */
export function send(name: String, data: any) {
  if (!name || typeof name !== 'string') {
    throw ('name错误')
  }
  const newData = [NaN, undefined, null].includes(data) ? '' : typeof data === 'string' ? data : JSON.stringify(data)
  localStorage.setItem(name, newData)
}
/**
* 跨标签通讯---取
* @param {string} name 缓存的keyName
* @param {*} callback 回调
*/
export function loacalReceive(name: String, callback: Function) {
  if (!name || typeof name !== 'string') {
    throw ('查询name错误')
  }
  window.addEventListener('storage', (e) => {
    if (e.key && e.key === name && e.newValue) {
      const value = JSON.parse(e.newValue)
      callback && callback(value)
      localStorage.removeItem(name)
    }
  })
}
/**
* 跨标签tab监听---取
* @param {string} name 缓存的keyName
* @param {*} callback 回调
*/
export function tabReceive(name: String, callback: Function) {
  if (!name || typeof name !== 'string') {
    throw ('查询name错误')
  }
  window.addEventListener("visibilitychange", function () {
    if (!document.hidden && name && localStorage[name]) {
      const value = typeof localStorage[name] === 'string' ? localStorage[name] : JSON.parse(localStorage[name])
      callback && callback(value)
    }
  })
}
/**
* //监听返回值
*  communicate.loacalReceive('aaaa', (e) => {
      console.log(e);
  })
  communicate.tabReceive('aaaa', (e) => {
      console.log(e);
  })
*/
/**
* //存值
*  communicate.send('aaaa', '123123123123')
*/