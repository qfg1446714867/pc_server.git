import { createVNode } from 'vue'
import { authPinia } from '@/stores';
import * as icons from '@ant-design/icons-vue'
const stykeMask = (dom) => {
    dom.style.background = 'rgba(0,0,0,0.5)';
    dom.style.width = '100vw';
    dom.style.height = '100vh';
    dom.style.position = 'absolute';
    dom.style.top = '0';
    dom.style.left = '0';
    dom.style.display = 'flex';
    dom.style.flexFlow = 'column';
    dom.style.justifyContent = 'center';
    dom.style.alignItems = 'center';
}
const styleContent = (dom, { color, size }: any) => {
    const authStore = authPinia()
    dom.style.padding = '12px';
    dom.style.textAlign = 'center';
    dom.style.color = color || authStore.color;
    dom.style.fontSize = size;

}
export const LOADING: any = {
    options: {
        text: '正在加载中...',
        icon: 'LoadingOutlined',
        color: '',
        size: '14px'
    },
    onSetOption: function (options: any) {
        return {
            options: { ...this.options, ...options },
            onOpen: this.onOpen,
            onClose: this.onClose
        }
    },
    onOpen: async function (...args) {
        const _this = this
        const loadMaskEl = document.querySelector('#app')
        const loadMask = document.createElement('div')
        loadMask.className = 'q-loading-content'
        stykeMask(loadMask)
        const itemContent = document.createElement('div')
        itemContent.className = 'q-loading-content-box'
        styleContent(itemContent, _this.options)
        const data = args && args.length > 0 ? args : ['span', null,
            [
                h(icons[_this.options.icon]),
                h('div', { style: { margin: '4px 0 0 0' } }, _this.options.text),
            ]
        ]
        const createVnodeLoad = createApp({
            render() {
                return createVNode(data[0], data[1], data[2])
            }
        });
        createVnodeLoad.mount(itemContent);
        loadMask.appendChild(itemContent)
        loadMaskEl?.appendChild(loadMask)
    },
    onClose: async () => {
        const loadMaskEl = document.querySelector('#app')
        const loadMask = document.querySelector('.q-loading-content')
        if (loadMask) {
            loadMaskEl?.removeChild(loadMask)
        }
    }
}