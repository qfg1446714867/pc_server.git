import { defineConfig, loadEnv, ConfigEnv, UserConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import Components from 'unplugin-vue-components/vite'
import { createSvgIconsPlugin } from "vite-plugin-svg-icons";
import AutoImport from 'unplugin-auto-import/vite'
import { resolve } from 'path';
import { AntDesignVueResolver } from "unplugin-vue-components/resolvers";
const pathSrc = resolve(__dirname, "src");
export default defineConfig(({ mode }: ConfigEnv): UserConfig => {
  const env = loadEnv(mode, process.cwd());
  return {
    plugins: [
      vue(),
      vueJsx(),
      createSvgIconsPlugin({
        iconDirs: [resolve(pathSrc, "assets/icons")],
        symbolId: 'icon-[dir]-[name]',
      }),
      AutoImport({
        imports: [
          'vue',
          'vue-router',
        ],
        dts: "src/auto-import.d.ts",
        resolvers: [AntDesignVueResolver()]
      }),
      Components({
        dirs: ['src/components/module'],
        resolvers: [AntDesignVueResolver({
          resolveIcons: true,
          cjs: true,
          importStyle: false
        })]
      }),

    ],
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `
              @use "@/assets/css/publicStyle.scss" as *;
            `,
        }
      },
    },
    resolve: {
      alias: {
        '@': pathSrc
      },
    },
    server: {
      port: 9999,
      host: '0.0.0.0',
      open: true,
      proxy: {
        [env.VITE_LOGIN_BASE]: {
          target: env.VITE_LOGIN_URL,
          secure: false,
          changeOrigin: true,
          rewrite: (path) => path.replace(new RegExp(env.VITE_LOGIN_BASE), ""),
          bypass(req, res, options) {
            const proxyUrl = new URL(options.rewrite(req?.url) || '', (options?.target) as string)?.href || ''
            res.setHeader('x-proxy-url', proxyUrl)
          }
        }
      },
    },
    build: {
      chunkSizeWarningLimit: 2000,
      minify: "terser",
      terserOptions: {
        compress: {
          keep_infinity: true,
          drop_console: true, // 生产环境去除 console
          drop_debugger: true, // 生产环境去除 debugger
        },
        format: {
          comments: false, // 删除注释
        },
      },
    },
    optimizeDeps: {
      include: [
        "vue",
        "vue-router",
        "pinia",
        "@vueuse/core",
      ]
    }
  }
})
