const path = require('path');
// 获取body参数
const getBodyCall = function (req) {
    return new Promise((resolve) => {
        let body = ''
        req.on('data', (chunk) => {
            body += chunk
        })
        req.on('end', () => {
            resolve(body ? { token: req.headers.authorization, ...(JSON.parse(body)) } : {})
        })
    })
}
// 获取params参数
const getParamsCall = function (req) {
    return new Promise((resolve) => {
        const { searchParams } = new URL(req.url, `http://${req.headers.host}`)
        let params = {}
        searchParams.forEach((value, key) => {
            params = { ...params, [key]: value }
        });
        resolve(params)
    })
}
// 延时
const getTimeoutNum = function () {
    const num = Math.random();
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(true)
        }, num * 1000);
    })
}
// server路径
const serverPath = path.join(__dirname, '..');
// 404999999
const not404999999 = function (res, meg) {
    res.status(404);
    res.send({
        code: 999999,
        message: meg,
        data: null
    })
}
const validateToken = (data, token) => {
    return new Promise((resolve) => {
        const itemUserData = data.find((item) => item.token === token)
        resolve(itemUserData)
    })
}
module.exports = {
    path,
    getBodyCall,
    getParamsCall,
    getTimeoutNum,
    not404999999,
    serverPath,
    validateToken
}