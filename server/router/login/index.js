const express = require('express')
const { getBodyCall, getTimeoutNum, not404999999, validateToken } = require('../../utils/getArg.js')
const AuthRouter = express.Router()
const WebSocket = require('ws');
const data = require('../../data/index.json')
// 创建 WebSocket 服务器
const loginWss = new WebSocket.Server({ noServer: true });
// 登录
AuthRouter.post('/login', async (req, res) => {
    await getTimeoutNum()
    const { username, password, token } = await getBodyCall(req)
    console.log(username, password,'aaaaaaaaaaa');
    const itemUserData = data.find(item => ((item.user.username === username && item.user.password === password) || token == item.token))
   
    if (itemUserData) {
        res.send({
            code: 200000,
            message: '登录成功！',
            data: {
                token: itemUserData.token,
            }
        })
        return
    }
    not404999999(res, '登录失败，账户密码错误！')
})
AuthRouter.post('/phone', async (req, res) => {
    await getTimeoutNum()
    const { phone, phoneCode } = await getBodyCall(req)
    const itemUserData = data.find(item => item.user.phone === phone && item.phoneCode === phoneCode)
    if (itemUserData) {
        res.send({
            code: 200000,
            message: null,
            data: {
                token: itemUserData.token,
            }
        })
        return
    }
    not404999999(res, '获取失败')
})
// 获取code
AuthRouter.post('/getPhoneCode', async (req, res) => {
    await getTimeoutNum()
    const { phone } = await getBodyCall(req)
    const itemUserData = data.find(item => item.user.phone === phone)
    if (itemUserData) {
        res.send({
            code: 200000,
            message: null,
            data: itemUserData.phoneCode
        })
        return
    }
    not404999999(res, '获取失败')
})
// 获取二维码地址
AuthRouter.post('/getPhoneCode', async (req, res) => {
    await getTimeoutNum()
    const { phone } = await getBodyCall(req)
    const itemUserData = data.find(item => item.user.phone === phone)
    if (itemUserData) {
        res.send({
            code: 200000,
            message: null,
            data: itemUserData.phoneCode
        })
        return
    }
    not404999999(res, '获取失败')
})
let currentWs = null
loginWss.on('connection', async function connection(ws, req) {
    currentWs = ws
    ws.on('message', function incoming(message) {
    });
    ws.send(JSON.stringify({ id: 123456 }))
    ws.on('close', function () {
        console.log('login关闭链接-server');
    });
});
// 获取二维码地址
AuthRouter.post('/h5/token', async (req, res) => {
    await getTimeoutNum()
    const { id } = await getBodyCall(req)
    if (id == 123456) {
        res.send({
            code: 200000,
            message: null,
            data: 'ok'
        })
        currentWs.send(JSON.stringify({ type: 'success', data: 19980503 }));
        return
    }
    not404999999(res, '未知异常')
})
// 导出路由模块
module.exports = { AuthRouter, loginWss };