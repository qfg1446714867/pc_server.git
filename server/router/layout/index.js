const express = require('express')
const fs = require('fs')
const { getBodyCall, getTimeoutNum, serverPath, not404999999, validateToken } = require('../../utils/getArg.js')
const router = express.Router()
const data = require('../../data/layout.json')
const user = require('../../data/index.json')
// 获取message
router.post('/getMessageList', async (req, res) => {
    await getTimeoutNum()
    const { token } = await getBodyCall(req);
    const itemUserData = await validateToken(user, token);
    if (itemUserData) {
        const message = data.messageObj
        res.send({
            code: 200000,
            message: '请求成功！',
            data: {
                list: message.list,
                total: message.list.length
            }
        })
        return
    }
    not404999999(res, '请求失败！')
})
// 阅读
router.post('/getReadMessage', async (req, res) => {
    await getTimeoutNum()
    const { token, id } = await getBodyCall(req)
    const itemUserData = await validateToken(user, token)
    if (itemUserData) {
        const newData = {
            messageObj: {
                list: [
                    ...data.messageObj.list.map((n) => {
                        if (n.id === id) (n.read = true)
                        return n
                    })
                ]
            }
        }
        fs.writeFileSync(`${serverPath}/data/layout.json`, JSON.stringify(newData))
        res.send({
            code: 200000,
            message: '请求成功！',
            data: null
        })
        return
    }
    not404999999(res, '请求失败！')
})
module.exports = router