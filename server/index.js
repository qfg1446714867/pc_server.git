const express = require('express')
const app = express()
const http = require('http');
const server = http.createServer(app);
const chartServer = http.createServer(app);
const loginServer = http.createServer(app);

const {AuthRouter, loginWss} = require('./router/login/index.js')
const UserRouter = require('./router/user/index.js')
const LayoutRouter = require('./router/layout/index.js')
const HomeRouter = require('./router/home/index.js')
const { wss } = require('./router/socket/index.js');
const { chartWs } = require('./router/socket/charts.js');
const XcxRouter = require('./xcxRouter/index.js')
app.use(AuthRouter)
app.use(UserRouter)
app.use(LayoutRouter)
app.use(HomeRouter)
app.use(XcxRouter)

server.on('upgrade', function (request, socket, head) {
    wss.handleUpgrade(request, socket, head, function (ws) {
        wss.emit('connection', ws, request);
    });
});
chartServer.on('upgrade', function (request, socket, head) {
    chartWs.handleUpgrade(request, socket, head, function (ws) {
        chartWs.emit('connection', ws, request);
    });
});
loginServer.on('upgrade', function (request, socket, head) {
    loginWss.handleUpgrade(request, socket, head, function (ws) {
        loginWss.emit('connection', ws, request);
    });
});
app.listen(1998, () => console.log('服务启动：1998'))
server.listen(1999, () => console.log('服务启动：1999'))
chartServer.listen(2000,()=>console.log('服务启动：2000'))
loginServer.listen(2001, ()=>console.log('服务启动：2001'))
